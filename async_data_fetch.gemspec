$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "async_data_fetch/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "async_data_fetch"
  s.version     = AsyncDataFetch::VERSION
  s.authors     = ["Leo Benkel"]
  s.email       = ["leo.benkel@gmail.com"]
  s.homepage    = "https://gitlab.com/wonay/async_data_fetch"
  s.summary     = "Easy way to fetch data asynchronously"
  s.description = "Easy way to fetch data asynchronously"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.required_ruby_version = ">= 2.3.0"

  s.add_dependency "rails", ">= 4.2.4"

  s.add_dependency 'jquery-rails', '>= 4.1.1'
  s.add_dependency  'rails-i18n', '~> 4.0.0'

  s.add_development_dependency "sqlite3"
  s.add_development_dependency 'byebug'
  s.add_development_dependency 'spring'
end
