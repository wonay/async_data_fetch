$(document).ready(function(){
	Queries = []
	$('.fetch_data_async').each(function(i, elem){
		var model_name = $(elem).data().model_name;
		var model_id = $(elem).data().model_id;
		var model_method = $(elem).data().model_method;
		var display_error = $(elem).data().display_error;

		var func_done = function(data){
			//console.log("SUCCEED: " + data.success)
			$(elem).text(data.success);
		};
		var func_error = function(data){
			if(display_error) {
				if (data.responseJSON){
					console.log("ERROR: " + data.responseJSON.error);
					$(elem).text("ERROR: " + data.responseJSON.error);
				} else {
					console.log("ERROR: " + JSON.stringify(data));
				}
			} else {
				$(elem).text("FAILED");
				console.log("You can use 'display_error: true' to get more detailed on the error.")
			}
		};
		var func_always = function(data){
			delete Queries[i];
			console.log("Delete query #" + i + ", left: " + Queries);
		};

		query = $.ajax({
			url: '/async_data_fetch/get_async_data',
			data: 'model_name=' + model_name + "&model_id=" + model_id + "&model_method=" + model_method,
			method: 'post',
			async: true,
			success: func_done,
			done: func_done,
			error: func_error,
			fail: func_error,
			complete: func_always,
			always: func_always
		});

		Queries[i] = query;
	});

	var kill_ajax = function() {
		$.each(Queries, function(i, query){
			if (!query){
				return;
			}
			console.log("Abort BEFORE: #" + i + ": " + JSON.stringify(query));
			result = query.abort();
			console.log("Abort AFTER: #" + i + ": " + JSON.stringify(result));
		});
	};

	$(window).bind('beforeunload', kill_ajax);
	$(window).on('unload', kill_ajax);
});
