require 'test_helper'

class ActsAsAsyncTest < ActiveSupport::TestCase

	def test_hickwalls_as_async
		assert_equal ["last_squawk", "test_method"], Hickwall.available_methods
	end

	def test_wickwalls_as_async
		assert_equal ["plop"], Wickwall.available_methods
	end
end
