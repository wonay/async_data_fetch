class Wickwall < ActiveRecord::Base
	acts_as_async :plop, :long_time

	def plop
		return 12
	end

	def long_time
		sleep(20)
		return "done"
	end
end
