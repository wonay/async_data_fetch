module AsyncDataFetch
	module ViewHelpers
		def fetch_async(model_name: , model_id: , model_method: , tag: "span", placeholder: "?", display_error: false, extra_classes: "")

			if !!display_error != display_error then
				raise "display_error has to be a boolean."
			end

			render partial: 'async_data_fetch/fetch_async_data', locals: {_tag: tag, _model_name: model_name.to_s, _model_id: model_id.to_s, _model_method: model_method.to_s, _placeholder: placeholder.to_s, _class: 'fetch_data_async ' + extra_classes.to_s, _display_error: display_error }
		end
	end
end

ActionView::Base.send :include, AsyncDataFetch::ViewHelpers
