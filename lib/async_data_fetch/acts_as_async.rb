module AsyncDataFetch
	module ActsAsAsync
		extend ActiveSupport::Concern

		included do
		end

		module ClassMethods

			attr_accessor :async_data_fetch_available_methods

			def acts_as_async(*queries)
				cattr_accessor :async_data_fetch_available_methods
				self.async_data_fetch_available_methods = queries.map(&:to_s)
			end
		end
	end
end

ActiveRecord::Base.send :include, AsyncDataFetch::ActsAsAsync
