module AsyncDataFetch
  class Engine < ::Rails::Engine
  	isolate_namespace AsyncDataFetch

	initializer "async_data_fetch", before: :load_config_initializers do |app|
		Rails.application.routes.append do
			mount AsyncDataFetch::Engine, at: "/async_data_fetch"
		end
	end
  end
end
